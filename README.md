<h1>got</h1>

*You Git, we Got*

<br/>

## Overview
**Got** is a tool made to help increase productivity and decrease button pushing while using Git.  

**Got** works by providing short helpful asiasing of commands and easy configuration of those aliases.  

Unlike using regular [Git aliases](https://git-scm.com/book/en/v2/Git-Basics-Git-Aliases), **Got** provides easily configured dynamic references when working with Git.  Got takes your Git aliasing and productivity to the next level.  

<br/>

# Example Usage
Here's a common use case of pushing a new branch to remote `origin`:  
```bash
# Got command
got p

# Equivalient Git command
git push -u origin <current branch name>
```

Got will auto-detect whether you are using `main` or `master` in your repo:
```bash
# Got command
got m

# Equivalient Git command
git checkout master OR git checkout main
```

Here's another common case of pulling latest from master/main branch and then merging into current branch:  
```bash
# Got command
got mPlgm

# Equivalient Git commands (master or main)
git checkout master && \
    git pull && \
    git checkout <last branch name> && \
    git merge master
```
For those about to pull w/rebase, we salute you:  
```bash
# Got command
got mPrlgm

# Equivalient Git commands (master or main)
git checkout master && \
    git pull -r && \
    git checkout <last branch name> && \
    git merge master
```

**Got** supports shortcuts too:  
```bash
# Got command which stores current branch as shortcut '1'
got -- sc 1 :cb:

# Got commands
got k/feature/my-new-feature
got mprk:1:gm

# Equivalient Git commands (master or main)
git checkout -b feature/my-new-feature && \
    git checkout master && \
    git pull -r && \
    git checkout <original branch name> && \
    git merge master
```

<br/>

## Installation
Currently, the fastest way to install **Got** is to have Go installed and run `go get -u gitlab.com/metalgopher/got`.  
If your `GOPATH` is part of your system path then there is nothing more you need to do.  

If you do not have Go installed you can download latest relese from repo packages.  

<br/>

## Prerequisites
Have Git installed.  

<br/>

## Setup
To use default configuration, there is no further setup required.  To run with a custom setup, read on.  

**Got** uses the TOML config file format.  To run with custom configuration, first generate a default config file by running `got -i cfgout .` which will write the default config file `got.toml` to the current directory.  

<br/>

## Standard Commands
This section outlines standard (default) commands offered by **Got**

<br/>

### k
- usage: `got k/<ref name/shortcut>`  
  - This command checks out the branch or ref, creating a new branch if necessary

### l
- usage: `got l`  
  - This command checks out the last branch that was checked out via **Got**
  - For example, if you run `got k/foo && got k/bar && got l`, you will be back on branch `foo`

### m
- usage: `got m`  
  - This command checks out the `main` or `master` branch, whichever exists
  - If both a main and a master branch exist in the current repo, default is used (configurable)

### p
- usage: `got p`  
  - This command pushes the current branch to the default remote named `origin`. 
  - Upstream is set automatically as `origin/<branch name>` if not already set

### P
- usage: `got P`  
  - This command pulls the current upstream tracking branch to the current branch. 
  - If no upstream tracking branch is set, this command pulls from remote named `origin` with current branch name

<br/>

## Sub-Commands
This section outlines sub-commands that are more for configuring **Got** than running Git.  All **Got** sub-commands start with `got -- `  

<br/>

### cfgout
- usage: `got -- cfgout [<path>]`  
  - This command writes the current **Got** config as a TOML file to the specified path.  
  - Default configuration will be produced unless `~/.got/config.toml` exists, in which case a copy of that file will be written
  - The `<path>` argument is optional.  If path is not provided, current directory is used.  If path is specified as a directory, `got.toml` will be created there.  If specified as a directory + file name, file will be created there as named unless the name does not end in `.toml` (case insensitive), in which case the `.toml` extension will be appended.  
  - For example, the following two commands produce the same `got.toml` file in the current directory.  
    - `got -- cfgout .` => writes `./got.toml`
    - `got -i cfgout got` => writes `./got.toml`

### cfgin
- usage: `got -- cfgin [<path>]`  
  - This command reads TOML config file from the specified path, parses the file, validates it, and updates `~/.got/config.toml` 
  - The `<path>` argument is optional.  If path is not provided, `got.toml` will be read from current directory.  If path is specified as a directory, `got.toml` will be read from that directory.  If specified as a directory + file name, file will be read as a TOML file regardless of actual file name.  
  - For example, the following two commands read the same `got.toml` file from the current directory.  
    - `got -- cfgin .` => reads `./got.toml`
    - `got -i cfgin` => reads `./got.toml`
